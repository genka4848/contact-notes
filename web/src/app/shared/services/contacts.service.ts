import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Contact } from '../models';

const API_PREFIX = '/api';

@Injectable({
  providedIn: 'root'
})
export class ContactsService {

  constructor(private http: HttpClient) { }

  find() {
    return this.http.get<Contact[]>(`${API_PREFIX}/contacts`);
  }

  get(contactId: number) {
    return this.http.get<Contact>(`${API_PREFIX}/contacts/${contactId}`);
  }

  create(contact: Contact) {
    return this.http.post<Contact>(`${API_PREFIX}/contacts`, contact);
  }
}
