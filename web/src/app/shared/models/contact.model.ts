
export interface Contact {
  id?: number;
  name: string;
  email: string;
  gender: 'male' | 'female';
  budget: number;
  birthDate: string;
}
