import { Component, OnInit, ViewChild } from '@angular/core';
import { finalize } from 'rxjs/operators';

import { ContactsService } from '../../shared/services/contacts.service';
import { ContactFormComponent } from '../contact-form/contact-form.component';

@Component({
  selector: 'app-contact-add',
  templateUrl: './contact-add.component.html',
  styleUrls: ['./contact-add.component.scss']
})
export class ContactAddComponent implements OnInit {

  @ViewChild(ContactFormComponent, { static: false })
  private contactForm: ContactFormComponent;

  public isSaving = false;

  constructor(private contactsService: ContactsService) { }

  ngOnInit() {
  }

  add(newContact: any) {
    if (this.isSaving) {
      return;
    }

    this.isSaving = true;
    this.contactsService
      .create(newContact)
      .pipe(finalize(() => {
        this.isSaving = false;
        this.contactForm.reset();
      }))
      .subscribe();
  }
}
