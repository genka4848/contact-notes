import { Component, Input, Output, OnChanges, SimpleChanges, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { Contact } from '../../shared/models';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss']
})
export class ContactFormComponent implements OnChanges {

  @Input() public contact: Contact;
  @Input() public viewMode = false;

  @Output() public save = new EventEmitter<Contact>();

  public form: FormGroup;

  constructor() {
    this.form = new FormGroup({
      name: new FormControl(null),
      email: new FormControl(null, [Validators.email]),
      budget: new FormControl(null),
      gender: new FormControl('male'),
      birthDate: new FormControl(null),
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.contact && changes.contact.currentValue) {
      this.form.patchValue(changes.contact.currentValue);
    }
  }

  submit() {
    if (this.form.valid) {
      this.save.emit(this.form.value);
    }
  }

  reset() {
    this.form.reset({ gender: 'male' });
    this.form.markAsPristine();
    this.form.markAsUntouched();
  }
}
