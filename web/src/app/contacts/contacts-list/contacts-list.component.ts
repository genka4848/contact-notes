import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ContactsService } from '../../shared/services/contacts.service';
import { Contact } from '../../shared/models';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts-list.component.html',
  styleUrls: ['./contacts-list.component.scss']
})
export class ContactsListComponent implements OnInit {

  public contacts$: Observable<Contact[]>;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private contactsService: ContactsService
  ) { }

  ngOnInit() {
    this.contacts$ = this.contactsService.find();
  }

  openContactDetails(contactId: number) {
    this.router.navigate(['./details', contactId], { relativeTo: this.route });
  }
}
