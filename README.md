# Contact notes

Small test CRUD app

## Installation
1. ### Install server dependencies
```bash
cd api
node ci
```
2. ### Install client dependencies
```bash
cd web
node ci
```
3. ### Run server ENV via **docker-compose**
```bash
docker-compose up -d
```
4. ### Run client app
```bash
cd web
npm start
```