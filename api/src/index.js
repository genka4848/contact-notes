const feathers = require('@feathersjs/feathers');
const express = require('@feathersjs/express');
const configuration = require('@feathersjs/configuration');

const connectToDB = require('./providers/db');
const initContactModule = require('./modules/contact/');

const app = express(feathers());
app.configure(configuration());

const connection = connectToDB(app.get('db')); 

app.use(express.json())
app.use(express.urlencoded({ extended: true }));
app.configure(express.rest());

initContactModule(app, connection);

app.use(express.notFound());
app.use(express.errorHandler());

module.exports = app;