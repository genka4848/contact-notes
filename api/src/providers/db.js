const Sequelize = require('sequelize');

module.exports = ({ database, host, port, user, password }) => {
  console.log(database, host, port, user, password);
  return new Sequelize(
  database,
  user,
  password, {
    dialect: 'mysql',
    host: host,
    port: port
  }
)};