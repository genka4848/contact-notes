const Sequelize = require('sequelize');

const { Model } = Sequelize;

const attributes = {
  id: {
    type: Sequelize.BIGINT,
    primaryKey: true,
    autoIncrement: true
  },
  name: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: { notEmpty: true },
    set(val) { this.setDataValue('name', val.trim()) }
  },
  email: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: { isEmail: true }
  },
  budget: {
    type: Sequelize.BIGINT,
    allowNull: false,
    validate: { min: 0 }
  },
  gender: {
    type: Sequelize.ENUM,
    values: ['male', 'female'],
    allowNull: false
  },
  birthDate: {
    type: Sequelize.DATEONLY,
    allowNull: false,
    validate: { isDate: true }
  }
}

class ContactModel extends Model {}

module.exports = (sequelize) => {
  const instance = ContactModel.init(attributes, { sequelize, tableName: 'contacts' });
  instance.sync({ force: false });

  return instance;
};