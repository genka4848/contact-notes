const service = require('feathers-sequelize');

const contactServiceHooks = require('./hooks');
const contactModelFactory = require('./model');

module.exports = (app, sequelize) => {
  app.use(`${app.get('api_prefix')}/contacts`, service({ Model: contactModelFactory(sequelize) }));
  app.service(`${app.get('api_prefix')}/contacts`).hooks(contactServiceHooks)
};