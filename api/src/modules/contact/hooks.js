const { BadRequest } = require('@feathersjs/errors');

module.exports = {
  before: {
    find(context) {
      context.params.query.$sort = { createdAt: -1 };
    },
    async create({ service, data }) {
      const { email } = data;
      const contacts = await service.find({ query: { email } });

      if (contacts && contacts.length) {
        throw new BadRequest(`Contact with provided email already exists!`, { email });
      }
    }
  }
};