const app = require('./src')
const os = require('os')

const { port } = app.get('server');
const server = app.listen(port);

process.on('unhandledRejection', (reason, p) =>
  console.error('Unhandled Rejection', p, reason)
);

process.on('exit', function (){
  console.log('Process exit!');
});

server.on('listening', () => console.log(`Feathers REST API started at Port:${port}`));